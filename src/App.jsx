import './components/Potions.css'
import './App.css'
import Potions from './components/Potions'
import TitleH3 from './components/TitleH3'

function App() {

  return (
    <>
      <TitleH3 
      title="Les potions top tendance"
      color=""/>
      <section className='potion-container'>
        <Potions
        title = "Breuvage de Vitalité"
        ratings = {4}
        reviews = {11}
        picture = "https://i.etsystatic.com/29932636/r/il/3ed390/4944348682/il_570xN.4944348682_e808.jpg"
        />
        <Potions
        title = "Potion de Soin"
        ratings = {5}
        reviews = {26}
        picture = "https://i.etsystatic.com/29932636/r/il/0e4b14/4992583189/il_794xN.4992583189_tp7g.jpg"
        />
        <Potions
        title = "Philtre de Magie"
        ratings = {2}
        reviews = {1}
        picture = "https://as2.ftcdn.net/v2/jpg/02/44/37/57/1000_F_244375797_2qiVM6jdmbifuC1NRCfbfzxsUBWekHFc.jpg"
        />
      </section>
    </>
  )
}

export default App
