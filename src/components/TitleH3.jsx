function TitleH3 (props) {
    return <h3 className="title-h3" style={{color: props.color}}> {props.title} </h3>
}

export default TitleH3