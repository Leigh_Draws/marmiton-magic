import { useState } from "react";
import HeartFull from "../assets/heart-full.png";
import HeartEmpty from "../assets/heart-empty.png";
import '../components/Like.css'

export default function Like() {
  const [like, setLike] = useState(false);

  function toggleLike() {
    if (like) {
      setLike(false);
    } else {
      setLike(true);
    }
  }

  return (
    <>
      <input
        className="like-button"
        id="add-button"
        type="image"
        alt="ajouter aux favoris"
        src={like ? HeartFull : HeartEmpty}
        onClick={toggleLike}
      />
    </>
  );
}
