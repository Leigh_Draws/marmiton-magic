import Like from "./Like";

function Potions(props) {
  return (
    <article>
      <div className="potion-card">
        <div className="potion-infos">
          <Like />
          <h3>{props.title}</h3>
          <p className="potion-infos-small">{props.ratings} /5</p>
          <p className="potion-infos-small">{props.reviews} avis</p>
        </div>
        <div className="potion-filter"></div>
        <img
          className="potion-picture"
          src={props.picture}
          alt={props.title}
        ></img>
      </div>
    </article>
  );
}

export default Potions;

// onClick={() => {like === false ? setLike (true) : false} }
